const express = require('express');
const app = express();
const port = 8000;

const Vigenere = require('caesar-salad').Vigenere;

const encode = (letter) => {
    return Vigenere.Cipher('password').crypt(`${letter}`);
};

const decode = (encode) => {
    return Vigenere.Decipher('password').crypt(`${encode}`);
};

app.get('/', (req, res) => {
    res.send('<h1>Home Page</h1>');
});

app.get('/:hello', (req, res) => {
    res.send(`<h1>${req.params.hello}</h1>`);
});

app.get('/encode/password/:letter', (req, res) => {
    res.send(`<h1>${encode(req.params.letter)}</h1>`);
});

app.get(`/decode/password/:encode`, (req, res) => {
    res.send(`<h1>${decode(req.params.encode)}</h1>`);
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});

